package assignment04;

public interface GraphIF {
     
	/**
	 * @return 
	 * 		Returns the number of vertices in the graph. 
	 */
	public int getNumberOfVertices();
	
	/**
	 * @return 
	 * 		Returns the number of edges in the graph.
	 */
	public int getNumberOfEdges();

    /**
     * @return 
     * 		Returns an array of length getNumberOfVertices() with the inserted vertices. 
     */
	public MyVertex[] getVertices();
	
    /**
     * @return Returns an array of length getNumberOfEdges() with the inserted edges.
     */
	public MyEdge[] getEdges();
	

	/**
	 * Inserts a new vertex v into the graph and returns its index in the vertex array.
	 * If the vertex array is already full, then the method doubleArraySize() shall be called 
	 * before inserting. 
     * Null parameter is not allowed (IllegalArgumentException).
     * 
	 * @param v 
	 * 		the vertex to be inserted
	 * @return 
	 * 		index in the vertex array.
	 * @throws IllegalArgumentException if param v is null.
	 */
	public int insertVertex(MyVertex v) throws IllegalArgumentException;


	/**
	 * Returns the edge weight if there is an edge between index v1 and v2, otherwise -1.
	 * In case of unknown or identical vertex indices throw an IllegalArgumentException.
	 * @param v1 
	 * 		vertex 1
	 * @param v2 
	 * 		vertex 2
	 * @return 
	 * 		Returns the edge weight or -1 if there is no edge.
	 * @throws IllegalArgumentException
	 */
	public int hasEdge(int v1, int v2) throws IllegalArgumentException;
	

	/**
	 * Inserts an edge between vertices with v1 and v2. False is returned if the edge already exists,
	 * true otherwise. An IllegalArgumentException shall be thrown if the vertex indices are unknown (out of range) or
	 * if v1 == v2 (loop).
	 * 
	 * @param v1 
	 * 		vertex 1
	 * @param v2 
	 * 		vertex 2
	 * @param weight
	 * 		weight of the edge 
	 * @return
	 * 		Returns false if the edge already exists, true otherwise.
	 * @throws IllegalArgumentException
	 * 		If one of the vertices is out of the allowed range if there is a loop.
	 */
	public boolean insertEdge (int v1, int v2, int weight)  throws IllegalArgumentException;
	
	/**
	 * @return
	 * 		Returns an NxN adjacency matrix for the graph, where N = getNumVertices().
     * 		The matrix contains 1 if there is an edge at the corresponding index position, otherwise 0.
	 */
	public int[][] getAdjacencyMatrix();

	
	/**
	 * Returns an array of vertices which are adjacent to the vertex with index v.
	 * 
	 * @param v
	 * 		The vertex of which adjacent vertices are searched.
	 * @return
	 * 		array of adjacent vertices to v.
	 * @throws IllegalArgumentException
	 * 		If the vertex index v is unknown an IllegalArgumentException shall be thrown.
	 */
	public MyVertex[] getAdjacentVertices(int v) throws IllegalArgumentException;
	
	
	
	//-------------------------------------------------------------------
	//--------- Example 2 Methods
	
    /**
     * @return
     * 		Returns true if the graph is connected, otherwise false. 
     */
	public boolean isConnected();

 
	/**
	 * @return
	 *		Returns the number of all (weak) components  
	 */
	public int getNumberOfComponents();


	/**
	 * 	Prints the vertices of all components (one line per component).
	 *  E.g.: A graph with 2 components (first with 3 vertices, second with 2 vertices) should look like:
	 *	 [vertex1] [vertex2] [vertex3]
	 *   [vertex4] [vertex5]
	 */
	public void printComponents();

	
	/**
	 * @return
	 * 		Returns true if the graphs contains cycles, otherwise false. 
	 */
	public boolean isCyclic();
}
