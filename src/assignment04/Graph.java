package assignment04;

import java.util.ArrayList;
import java.util.Arrays;

public class Graph implements GraphIF {

	protected MyVertex[] vertices;    // vertex array
	protected MyEdge[] edges;        // edge array

	protected int numVertices;
	protected int numEdges;

	// doubles the array
	private void doubleArraySize() {
		int arraySize = vertices.length;
		vertices = Arrays.copyOf(vertices, arraySize * 2);
		edges = Arrays.copyOf(edges, arraySize * 2 * (arraySize * 2 - 1));
	}
    
	// Creates an empty graph.	
	public Graph() {
		vertices = new MyVertex[1];
		edges = new MyEdge[0];
		
		numVertices = 0;
		numEdges = 0;
	}

	/**
	 * @return 
	 * 		Returns the number of vertices in the graph. 
	 */
	public int getNumberOfVertices() {
		return numVertices;
	}
	
	/**
	 * @return 
	 * 		Returns the number of edges in the graph.
	 */
	public int getNumberOfEdges() {
		return numEdges;
	}

    /**
     * @return 
     * 		Returns an array of length getNumberOfVertices() with the inserted vertices. 
     */
	public MyVertex[] getVertices() {
		return Arrays.copyOf(vertices, numVertices);
	}
	
    /**
     * @return Returns an array of length getNumberOfEdges() with the inserted edges.
     */
	public MyEdge[] getEdges() {
		return Arrays.copyOf(edges, numEdges);
	}
	

	/**
	 * Inserts a new vertex v into the graph and returns its index in the vertex array.
	 * If the vertex array is already full, then the method doubleArraySize() shall be called 
	 * before inserting. 
     * Null parameter is not allowed (IllegalArgumentException).
     * 
	 * @param v 
	 * 		the vertex to be inserted
	 * @return 
	 * 		index in the vertex array.
	 * @throws IllegalArgumentException if param v is null.
	 */
	public int insertVertex(MyVertex v) throws IllegalArgumentException {
		if (v == null) {
			throw new IllegalArgumentException(); /* "Value v must not be null!" */
		}
		if (numVertices >= vertices.length) {
			doubleArraySize();
		}
		vertices[numVertices] = v;
		int index = numVertices;
		numVertices++;

		return index;
	}

	private boolean[] visited;                /* Needed for the DFS-Alg. to avoid endless cycles */


	/**
	 * Inserts an edge between vertices with v1 and v2. False is returned if the edge already exists,
	 * true otherwise. An IllegalArgumentException shall be thrown if the vertex indices are unknown (out of range) or
	 * if v1 == v2 (loop).
	 *
	 * @param v1
	 * 		vertex 1
	 * @param v2 
	 * 		vertex 2
	 * @param weight
	 * 		weight of the edge 
	 * @return
	 * 		Returns false if the edge already exists, true otherwise.
	 * @throws IllegalArgumentException
	 * 		If one of the vertices is out of the allowed range if there is a loop.
	 */
	public boolean insertEdge (int v1, int v2, int weight)  throws IllegalArgumentException {
		if (vertices.length < v1 || vertices.length < v2) {
			throw new IllegalArgumentException(); /* "Index v1 or v2 out of bound" */
		}
		if (v1 == v2) {
			throw new IllegalArgumentException(); /* "Loop detected in between v1 & v2" */
		}
		if (hasEdge(v1, v2) > 0) {
			return false; /* "There already exists an edge between v1 & v2" */
		}

		MyEdge curr = new MyEdge();
		curr.in = v1;
		curr.out = v2;
		curr.weight = weight;

		edges[numEdges] = curr;
		numEdges++;

		return true;
	}

	/**
	 * Returns the edge weight if there is an edge between index v1 and v2, otherwise -1.
	 * In case of unknown or identical vertex indices throw an IllegalArgumentException.
	 *
	 * @param v1 vertex 1
	 * @param v2 vertex 2
	 * @return Returns the edge weight or -1 if there is no edge.
	 * @throws IllegalArgumentException will throw an Exception if v1 || v2  negative
	 */
	public int hasEdge(int v1, int v2) throws IllegalArgumentException {
		if (v1 < 0 || v2 < 0) {
			throw new IllegalArgumentException(); /* "Value v must not be negative!" */
		}
		if (numEdges < 1) {
			return -1;
		}

		for (MyEdge e : edges) {
			if (e == null) {
				return -1;
			}

			if (e.in == v1 && e.out == v2 || e.in == v2 && e.out == v1) {
				return e.weight;
			}
		}
		return -1;
	}

	
	/**
	 * Returns an array of vertices which are adjacent to the vertex with index v.
	 * 
	 * @param v
	 * 		The vertex of which adjacent vertices are searched.
	 * @return
	 *        array of adjacent vertices to v.
	 * @throws IllegalArgumentException
	 * 		If the vertex index v is unknown an IllegalArgumentException shall be thrown.
	 */
	public MyVertex[] getAdjacentVertices(int v) throws IllegalArgumentException {
		if (v >= numVertices) {
			throw new IllegalArgumentException(); /* "Value v does not exist!" */
		}

		ArrayList<MyVertex> adj = new ArrayList<>();
		int[][] adjMatrix = getAdjacencyMatrix();
		for (int i = 0; i < numVertices; i++) {
			int curr = adjMatrix[v][i];
			if (curr == 1) {
				adj.add(vertices[i]);
			}
		}
		return (MyVertex[]) adj.toArray();
	}


	//-------------------------------------------------------------------
	//--------- Example 2 Methods

	/**
	 * @return Returns true if the graph is connected, otherwise false.
	 */
	public boolean isConnected() {
		if (numVertices < 1) { /* "There are no Vertices!" */
			return false;
		}
		DFS(0);
		for (Boolean b : visited) {
			if (!b) {
				return false;
			}
		}
		return true;
	}


	/**
	 * @return Returns the number of all (weak) components
	 */
	public int getNumberOfComponents() {
		if (numVertices < 1) { /* "There are no Vertices!" */
			return 0;
		}
		DFS(0);
		return weakComponents;
	}

	/**
	 * @return Returns an NxN adjacency matrix for the graph, where N = getNumVertices().
	 * The matrix contains 1 if there is an edge at the corresponding index position, otherwise 0.
	 */
	public int[][] getAdjacencyMatrix() {
		int[][] adjMatrix = new int[numVertices][numVertices];

		/* Filling up the Array with default values */
		for (int[] matrix : adjMatrix) {
			Arrays.fill(matrix, 0);
		}

		Arrays.stream(edges).forEach(e -> adjMatrix[e.in][e.out] = 1);
		Arrays.stream(edges).forEach(e -> adjMatrix[e.out][e.in] = 1);
		return adjMatrix;
	}

	/**
	 * @return Returns true if the graphs contains cycles, otherwise false.
	 */
	public boolean isCyclic() {
		if (numEdges < 1) {
			return false;
		}
		DFS(0);
		return hasCycle;
	}

	// Auxiliary Methods & additional variables
	private boolean hasCycle;                /* Cycle yes = true */
	private int weakComponents;                /* Counter for single Vertexes (weak Component) */
	private boolean[] done;                    /* An Auxiliary array for printing out all SubGraphs */

	/**
	 * Prints the vertices of all components (one line per component).
	 * E.g.: A graph with 2 components (first with 3 vertices, second with 2 vertices) should look like:
	 * [vertex1] [vertex2] [vertex3]
	 * [vertex4] [vertex5]
	 */
	public void printComponents() {

		done = new boolean[numVertices];
		Arrays.fill(done, false);

		while (notDone() >= 0) {
			DFS(notDone());
			print(visited);
		}
	}

	/* Reset and set up  for DFS Algorithm*/
	private void DFS(int visitIndex) {
		weakComponents = 0;
		hasCycle = false;
		visited = new boolean[numVertices];
		Arrays.fill(visited, false);

		subMethodDFS(visitIndex);
	}

	/* The recursive DFS Algorithm */
	private void subMethodDFS(int visitIndex) {
		if (!visited[visitIndex]) {
			visited[visitIndex] = true;

			ArrayList<MyEdge> indexEdges = getEdgesOf(visitIndex);

			assert indexEdges != null;
			if (indexEdges.isEmpty()) { /* there are no connections */
				weakComponents++;
			} else {
				for (MyEdge e : indexEdges) {
					subMethodDFS(e.in);
					subMethodDFS(e.out);
				}
			}
		} else {
			hasCycle = true;
		}
	}

	/* This Method will provide all Edges to that specific Vertex */
	private ArrayList<MyEdge> getEdgesOf(int index) {
		if (index < 0) {
			throw new IllegalArgumentException(); /* "Value v must not be negative!" */
		}
		if (numEdges < 1) {
			return null;
		}

		ArrayList<MyEdge> indexEdges = new ArrayList<>();

		for (MyEdge e : edges) {
			if (e != null) {
				if (e.in == index || e.out == index) {
					indexEdges.add(e);
				}
			}
		}
		return indexEdges;
	}

	/* This method is needed for print Component.
	 *  It will check if all SubGraphs are printed already  */
	private int notDone() {
		for (int i = 0; i < done.length; i++) {
			if (!done[i]) {
				return i;
			}
		}
		return -1;
	}

	/* Helper Method that will simply print out all given Vertexes (marked as true)  */
	private void print(boolean[] visited) {

		for (int i = 0; i < visited.length; i++) {
			if (visited[i]) {
				done[i] = true;
				System.out.print("[" + vertices[i] + "] ");
			}
		}
		System.out.println(""); /* necessary for nextLine */
	}



}
