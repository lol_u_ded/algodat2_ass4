package assignment04;

public class MyEdge {
    public int in, out; 	// incoming and outgoing indices of the vertices the an edge connects
    public int weight;		// weight of the edge
}
