package assignment04;

import static org.junit.Assert.*;
import org.junit.Test;

public class TestAssignment04students {
	
	private Graph g;  
	
	//--------------------------------------
	//--- TEST FOR EXAMPLE1 
	
	@Test
	public void testInsertVertex() {
		Graph map = new Graph();
		
		MyVertex test = new MyVertex("Linz");
		int linz = map.insertVertex(test);
		
		assertEquals(test, map.vertices[linz]);
	}
	
	@Test
	public void testGetEdges() {
		Graph map = new Graph();
		
		int linz = map.insertVertex(new MyVertex("Linz"));
		int wien = map.insertVertex(new MyVertex("Wien"));
		int graz = map.insertVertex(new MyVertex("Graz"));
		
		map.insertEdge(linz, wien, 100);
		MyEdge[] test = map.getEdges();
		assertEquals(1, test.length);
		
		assertTrue(wien == test[0].in || wien == test[0].out);
		assertTrue(linz == test[0].out || linz == test[0].in);
		assertEquals(100, test[0].weight);
		
		map.insertEdge(wien, graz, 50);
		test = map.getEdges();
		assertEquals(2, test.length);
		
		assertTrue(wien == test[0].in || wien == test[0].out);
		assertTrue(linz == test[0].out || linz == test[0].in);
		assertEquals(100, test[0].weight);
		
		assertTrue(graz == test[1].in || graz == test[1].out);
		assertTrue(wien == test[1].out || wien == test[1].in);
		assertEquals(50, test[1].weight);
	}
	

	@Test
	public void testGetVertices() {
		g = this.initializeGraph2Components();
		
		MyVertex[] v = g.getVertices();
		assertTrue(v[0].toString().equals("Linz"));
		assertTrue(v[1].toString().equals("St.Poelten"));
		assertTrue(v[2].toString().equals("Wien"));
		assertTrue(v[3].toString().equals("Innsbruck"));
		assertTrue(v[4].toString().equals("Bregenz"));
		assertTrue(v[5].toString().equals("Eisenstadt"));
		assertTrue(v[6].toString().equals("Graz"));
		assertTrue(v[7].toString().equals("Klagenfurt"));
		assertTrue(v[8].toString().equals("Salzburg"));
		assertTrue(v[9].toString().equals("London"));
	}
	
	
	//--------------------------------------
	//--- TEST BSP2
	
	@Test
	public void testIsConnected() {
		g = this.initializeGraph2Components();
		assertFalse(g.isConnected());
	}
	

	@Test
	public void testPrintComponents() {
		System.out.println();

		System.out.println("- Test1: ");
		g = this.initializeGraph2Components();
		System.out.println("Expected:\n"
				+ "[Linz] [Wien] [Eisenstadt] [Graz] [Klagenfurt] [Innsbruck] [Bregenz] [Salzburg]\n"
				+ "[St.Poelten] [London]\n"
				+ "Student result: ");
		g.printComponents();
	}
	

	
	//--------------------------------------
	//--- PRIVATE Methods
	
	private Graph initializeGraph2Components() {
		Graph map = new Graph();
		
		int linz = map.insertVertex(new MyVertex("Linz"));
		int stpoelten = map.insertVertex(new MyVertex("St.Poelten"));
		int wien = map.insertVertex(new MyVertex("Wien"));
		int innsbruck = map.insertVertex(new MyVertex("Innsbruck"));
		int bregenz = map.insertVertex(new MyVertex("Bregenz"));
		int eisenstadt = map.insertVertex(new MyVertex("Eisenstadt"));
		int graz = map.insertVertex(new MyVertex("Graz"));
		int klagenfurt = map.insertVertex(new MyVertex("Klagenfurt"));
		int salzburg = map.insertVertex(new MyVertex("Salzburg"));
		
		map.insertEdge(linz, wien, 1);
		map.insertEdge(wien, eisenstadt, 2);
		map.insertEdge(wien, graz, 3);
		map.insertEdge(graz, klagenfurt, 4);
		map.insertEdge(bregenz, innsbruck, 5);
		map.insertEdge(klagenfurt, innsbruck, 6);
		map.insertEdge(salzburg, innsbruck, 7);
		
		int london = map.insertVertex(new MyVertex("London"));

		map.insertEdge(stpoelten, london, 10);

		return map;
	}
		
}
